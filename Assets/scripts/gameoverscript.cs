﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gameoverscript : MonoBehaviour
{
    public Text gameOverTxt;


    // Start is called before the first frame update
    void Start()
    {
        if(gamesettings.gameVictory)
        {
            gameOverTxt.text = gamesettings.gameOverVictoryText;
        }
        else
        {
            gameOverTxt.text = gamesettings.gameOverLoseText;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void goToMainMenu()
    {
        scenemng.LoadLevel("MainMenuScene");
    }

    public void restartLevel()
    {
        scenemng.LoadLevel("GameScene");
    }

}
