﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class celloperation
{
    private static List<cellObjectType> listOfActiveCells = new List<cellObjectType>();

    public static void addNewCell(GameObject cellObj, int cellNb)
    {
        listOfActiveCells.Add(new cellObjectType(cellObj, cellNb));
    }


    public static void removeCell(GameObject cellObj)
    {
        cellObjectType cellToFind = null;

        cellToFind = listOfActiveCells.Find(
            delegate (cellObjectType cell)
            {
                return cellObj == cell.Cell;
            }
            );


        if(cellToFind != null)
        {
            listOfActiveCells.Remove(cellToFind);
        }
    }

    public static bool cellExistsForNumber(int number)
    {
        cellObjectType cellToFind = null;

        cellToFind = listOfActiveCells.Find(
            delegate (cellObjectType cell)
            {
                return number == cell.CellNumber;
            }
            );

        if(cellToFind != null)
        {
            return true;
        }

        return false;
    }

    public static int getNumberForCell(GameObject cellObj)
    {
        cellObjectType cellToFind = null;

        cellToFind = listOfActiveCells.Find(
            delegate (cellObjectType cell)
            {
                return cellObj == cell.Cell;
            }
            );

        if (cellToFind != null)
        {
            return cellToFind.CellNumber;
        }

        throw new System.ArgumentException("No such cell found");
    }

    public static void removeAllCells()
    {
        listOfActiveCells.Clear();
    }

}
