﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gamelevelsettings
{
    public int numberOfCellsDestroyedToChangeDynamic;
    public int winCondition;
    public int numberOfAntiBody;
    public int dynChangeToEmptyCell;
    public int dynChangeToAntiBody;
    public float antiBodySpeed;

    public gamelevelsettings(int cellDestroyToDynChange, int winCond, int antibodyNb, int dynChangeToEmpty, int dynChangeToAB, float abSpeed)
    {
        numberOfCellsDestroyedToChangeDynamic = cellDestroyToDynChange;
        winCondition = winCond;
        numberOfAntiBody = antibodyNb;
        dynChangeToEmptyCell = dynChangeToEmpty;
        dynChangeToAntiBody = dynChangeToAB;
        antiBodySpeed = abSpeed;
    }
}
