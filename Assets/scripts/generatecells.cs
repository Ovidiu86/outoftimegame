﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class generatecells : MonoBehaviour
{
    public GameObject[] availableCellsArray;

    private float crtTime;

    public static bool increaseNumberOfMaxCells;

    private static bool generateEmptyCells;
    private static byte generateEmptyCellsCounter;

    private const byte generateEmptyCellInterval = 6;


    // Start is called before the first frame update
    void Start()
    {

        // Debug.Log("Screen size is : " + maxXScreen + " && " + maxYScreen);
        generateEmptyCellsCounter = 0;

    }

    // Update is called once per frame
    void Update()
    {
        
        if (gamesettings.numberOfActiveCells < gamesettings.crtNumberOfActiveCells)
        {
            if(!generateEmptyCells)
            {
                generateNormalCell();
            }
            else
            {
                if(generateEmptyCellsCounter % generateEmptyCellInterval == 0)
                {
                    generateEmptyCell();
                }
                else
                {
                    generateNormalCell();
                }
                generateEmptyCellsCounter++;
            }
        }
        
    }

    private void generateEmptyCell()
    {
        GameObject newCell;

        /* generate the last cell which should be the empty cell */
        newCell = generateObject.generateGameObject(availableCellsArray[availableCellsArray.Length - 1], gameObject, new Vector3() ,false);
        if(newCell != null)
        {

            newCell.transform.localScale = new Vector3(gamesettings.cellObjectLocalScaleValue, gamesettings.cellObjectLocalScaleValue, gamesettings.cellObjectLocalScaleValue);
            /* add cell to the list */
            celloperation.addNewCell(newCell, -1);
            gamesettings.numberOfActiveCells++;
        }
        
    }

    private void generateNormalCell()
    {
        int localCellIndex;
        GameObject newCell;

        /* generate a cell with a number on it */
        localCellIndex = Random.Range(0, availableCellsArray.Length - 2);
        newCell = generateObject.generateGameObject(availableCellsArray[localCellIndex], gameObject, new Vector3(), false);

        if(newCell != null)
        {
            newCell.transform.localScale = new Vector3(gamesettings.cellObjectLocalScaleValue, gamesettings.cellObjectLocalScaleValue, gamesettings.cellObjectLocalScaleValue);

            celloperation.addNewCell(newCell, localCellIndex);
            gamesettings.numberOfActiveCells++;
        }
    }

    public static void setGenerateEmptyCells(bool gen)
    {
        generateEmptyCells = gen;
    }

}
