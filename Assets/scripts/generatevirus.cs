﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class generatevirus : MonoBehaviour
{
    public GameObject virusObject;


    // Start is called before the first frame update
    void Start()
    {
        generateVirusObject();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void generateVirusObject()
    {
        GameObject newVirus;

        /* generate a cell with a number on it */
        newVirus = generateObject.generateGameObject(virusObject, gameObject, new Vector3(), true);
        if(newVirus != null)
        {
            newVirus.transform.localScale = new Vector3(gamesettings.virusObjectLocalScaleValue, gamesettings.virusObjectLocalScaleValue, gamesettings.virusObjectLocalScaleValue);
        }
    }

}
