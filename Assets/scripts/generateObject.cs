﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class generateObject : MonoBehaviour
{
    public static GameObject generateGameObject(GameObject cellObj, GameObject parent, Vector3 preDefPosition, bool genPreDefPosition)
    {
        GameObject newCellObject = null;
        Collider2D[] colliderCircleArray;
        Vector3 newPosition;

        if(!genPreDefPosition)
        {
            /* generate new position */
            newPosition = new Vector3(Random.Range(0 - gamesettings.maxXScreen, gamesettings.maxXScreen), Random.Range(0 - gamesettings.maxYScreenDown, gamesettings.maxYScreenUp), 1);
        }
        else
        {
            newPosition = preDefPosition;
        }
        

        /* make sure that a cell is not generated on top of another one */
        colliderCircleArray = Physics2D.OverlapCircleAll(newPosition, 1f);

        if (colliderCircleArray.Length == 0)
        {
            newCellObject = Instantiate(cellObj, newPosition, Quaternion.identity, parent.transform);

        }

        return newCellObject;
    }
}
