﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class gamesettings
{
    public const int initialTimerValue = 10;
    public const float cellObjectLocalScaleValue = 0.3f;
    public const float virusObjectLocalScaleValue = 0.2f;

    public static int crtCellNumberToDestroy = -1;

    public const int initNumberOfActiveCells = 10;
    public static int crtNumberOfActiveCells = 10;
    public static int numberOfActiveCells = 0;
    public static int crtNumberOfActiveAB = 0;

    public static int numberOfCellsDestroyedSoFar = 0;
    public const float antiBodyGenerationPadding = 0.3f;
    // public static int numberOfCellsToGameDynamicChange = 5;
    // public const int numberOfCellsDestroyedToChangeDynamic = 5;


    public const int lowCellNumber = 0;
    public const int highCellNumber = 9;

    public static int winConditionNumberOfCells;
    public static int levelSelected;

    public static int numberOfCellsToGameDynamicChange;

    public static string[] availableLevelsStringArray = { "Level 1", "Level 2", "Level 3", "Level 4", "Level 5"};
    public static gamelevelsettings[] levelSettingsList = { new gamelevelsettings(5,50,0,0,0,0f),
        new gamelevelsettings(4,60,0,3,0,0f),
        new gamelevelsettings(3,70,1,3,5,0.02f),
        new gamelevelsettings(2,80,1,2,3,0.05f),
        new gamelevelsettings(1,100,1,1,0,0.1f)
            };

    //public static int[] winConditionPerLevel = { 50, 60, 70, 80, 100 };


    public static float maxXScreen;
    public static float maxYScreenUp;
    public static float maxYScreenDown;

    public static bool gameVictory;

    public const string gameOverVictoryText = "Level Passed !";
    public const string gameOverLoseText = "Level Failed !";

}
