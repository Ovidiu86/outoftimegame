﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class antibodyscript : MonoBehaviour
{

    private const float speedX = 0.01f;
    private float crttime;

    private int minXPosition;
    private int maxXPosition;

    private float minXPositionFloat;
    private float maxXPositionFloat;

    private bool sign;

    // Start is called before the first frame update
    void Start()
    {

        minXPosition = (int)((0 - (gamesettings.maxXScreen - gamesettings.antiBodyGenerationPadding)) * 100);
        maxXPosition = (int)((gamesettings.maxXScreen - gamesettings.antiBodyGenerationPadding) * 100);

        maxXPositionFloat = gamesettings.maxXScreen - gamesettings.antiBodyGenerationPadding;
        minXPositionFloat = 0 - (gamesettings.maxXScreen - gamesettings.antiBodyGenerationPadding);
        /* check position */
        if ((int)(this.transform.position.x * 100) == (int)((gamesettings.maxXScreen - gamesettings.antiBodyGenerationPadding) * 100))
        {
            sign = false;
        }
        else
        {
            sign = true;
        }

        crttime = 0f;
    }

    // Update is called once per frame
    void Update()
    {

        if (gamemanagerscript.isGameRunning())
        {
            crttime += Time.deltaTime;

            if (crttime >= speedX)
            {
                if (sign)
                {
                    this.transform.position = Vector2.MoveTowards(transform.position, new Vector2(maxXPositionFloat, this.transform.position.y), gamesettings.levelSettingsList[gamesettings.levelSelected].antiBodySpeed);   
                }
                else
                {
                    this.transform.position = Vector2.MoveTowards(transform.position, new Vector2(minXPositionFloat, this.transform.position.y), gamesettings.levelSettingsList[gamesettings.levelSelected].antiBodySpeed);
                }

                if( (int)(this.transform.position.x * 100) >= maxXPosition)
                {
                    sign = false;
                }

                if ( (int)(this.transform.position.x * 100) <= minXPosition)
                {
                    sign = true;
                }
                crttime = 0f;
            }
        }

    }
}
