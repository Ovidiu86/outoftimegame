﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class generaterandomcellnumber : MonoBehaviour
{
    private static Text cellText;

    private const string textPrefix = "Attack cell ";


    // Start is called before the first frame update
    void Start()
    {
        cellText = GetComponent<Text>();

      //  generateNewRandomCellNumber();
    }

    public static void displayNewRandomCellNumber(int number)
    {       
        cellText.text = textPrefix + number.ToString();
    }    


}
