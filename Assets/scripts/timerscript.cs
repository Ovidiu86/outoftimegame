﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class timerscript : MonoBehaviour
{
    private static float countDownTimer;
    private float crtTime100ms;
    private Text timerText;

    private static float countDownTimerResetValue;
  
    // Start is called before the first frame update
    void Start()
    {
        timerText = this.GetComponent<Text>();
        timerText.text = gamesettings.initialTimerValue.ToString();
        countDownTimerResetValue = gamesettings.initialTimerValue;
        countDownTimer = countDownTimerResetValue;
        crtTime100ms = 0f;
;    }

    // Update is called once per frame
    void Update()
    {
        float localTime = Time.deltaTime;


        /* check if game is running */
        if(gamemanagerscript.isGameRunning())
        {
            crtTime100ms += localTime;

            /* update each 90ms */
            if (crtTime100ms >= 0.09f)
            {
                if (countDownTimer >= 0.0f)
                {
                    timerText.text = string.Format("{0:F1}", countDownTimer);
                    //   Debug.Log("Crt time is : " + timerText.text);

                    if (countDownTimer == 0.0f)
                    {
                        gamemanagerscript.endGame();
                    }

                }
                crtTime100ms = 0f;
            }

            if (countDownTimer > 0.0f)
            {
                countDownTimer -= localTime;
                if (countDownTimer < 0f)
                {
                    countDownTimer = 0.0f;
                }
            }
        }
    } 

    public static void resetTimer()
    {
        countDownTimer = countDownTimerResetValue;
    }

    public static void updateTimerTimeout(int seconds)
    {
        countDownTimerResetValue -= seconds;
        if(countDownTimerResetValue < 0)
        {
            countDownTimerResetValue = 0;
        }
    }
}
