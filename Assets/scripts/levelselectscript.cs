﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class levelselectscript : MonoBehaviour
{
      public GameObject dropDownObject;

    // Start is called before the first frame update
    void Start()
    {
        dropDownObject.GetComponent<Dropdown>().ClearOptions();
        foreach (string s in gamesettings.availableLevelsStringArray)
        {
            dropDownObject.GetComponent<Dropdown>().options.Add(new Dropdown.OptionData(s));
        }

        dropDownObject.GetComponent<Dropdown>().value = 0;
        dropDownObject.GetComponent<Dropdown>().RefreshShownValue();

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void levelSelected()
    {

        gamesettings.levelSelected = dropDownObject.GetComponent<Dropdown>().value;
        gamesettings.winConditionNumberOfCells = gamesettings.levelSettingsList[dropDownObject.GetComponent<Dropdown>().value].winCondition;

        scenemng.LoadLevel("GameScene");
    }
}
