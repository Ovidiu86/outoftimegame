﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gamemanagerscript : MonoBehaviour
{
    public GameObject[] listOfElementsForGameOver;
    public Text gameOverTxt;
    public Text levelSelectedTxt;
    public Text progressTxt;

    private const string progressPrefix = "Progress : ";

    private const int gameInit = 1;
    private const int gameOngoing = 2;
    private const int gameFinished = 3;

    private const int randomGenTimeout = 100;

    private static int gameManagerState;

    private static bool generateCellTextFirstTime = false;

    private const float padding = 0.5f;
    private const float paddingUp = 1f;

    private byte numberOfTimeDynChanged;


    void Start()
    {

        gamesettings.maxXScreen = ((float)Camera.main.orthographicSize) * Camera.main.aspect - padding;
        gamesettings.maxYScreenUp = ((float)Camera.main.orthographicSize) - padding - paddingUp;
        gamesettings.maxYScreenDown = ((float)Camera.main.orthographicSize) - padding;

        levelSelectedTxt.text = gamesettings.availableLevelsStringArray[gamesettings.levelSelected];

        progressTxt.text = progressPrefix + "0/" + gamesettings.winConditionNumberOfCells;

        gamesettings.numberOfCellsDestroyedSoFar = 0;
        gamesettings.numberOfActiveCells = 0;
        gamesettings.crtNumberOfActiveCells = gamesettings.initNumberOfActiveCells;
        generatecells.setGenerateEmptyCells(false);
        gamesettings.numberOfCellsToGameDynamicChange = gamesettings.levelSettingsList[gamesettings.levelSelected].numberOfCellsDestroyedToChangeDynamic;
        numberOfTimeDynChanged = 0;
        celloperation.removeAllCells();
        generateCellTextFirstTime = false;
        gamesettings.crtNumberOfActiveAB = 0;

        initializeGame();
    }

    void Update()
    {
        /* check if all initial cells were generated */
        if((!generateCellTextFirstTime) && (gamesettings.numberOfActiveCells == gamesettings.initNumberOfActiveCells))
        {
            generateNewCellNumberText();
            generateCellTextFirstTime = true;

        }
        if(gamesettings.numberOfCellsDestroyedSoFar == gamesettings.numberOfCellsToGameDynamicChange)
        {
            numberOfTimeDynChanged++;
            gamesettings.numberOfCellsToGameDynamicChange += gamesettings.levelSettingsList[gamesettings.levelSelected].numberOfCellsDestroyedToChangeDynamic;
 
            gamesettings.crtNumberOfActiveCells = gamesettings.initNumberOfActiveCells + gamesettings.numberOfCellsDestroyedSoFar / gamesettings.levelSettingsList[gamesettings.levelSelected].numberOfCellsDestroyedToChangeDynamic;
            timerscript.updateTimerTimeout(1);

            if((gamesettings.levelSettingsList[gamesettings.levelSelected].dynChangeToEmptyCell > 0) && (numberOfTimeDynChanged == gamesettings.levelSettingsList[gamesettings.levelSelected].dynChangeToEmptyCell))
            {
                generatecells.setGenerateEmptyCells(true);
            }
        }
        if(gamesettings.crtNumberOfActiveAB < gamesettings.levelSettingsList[gamesettings.levelSelected].numberOfAntiBody)
        {
            if ((gamesettings.levelSettingsList[gamesettings.levelSelected].numberOfAntiBody > 0) && (numberOfTimeDynChanged == gamesettings.levelSettingsList[gamesettings.levelSelected].dynChangeToAntiBody))
            {
                generateantibody.generateAntiBodyRequest();
                gamesettings.crtNumberOfActiveAB++;
            }
        }

        if(gamesettings.numberOfCellsDestroyedSoFar >= gamesettings.winConditionNumberOfCells)
        {
            gamesettings.gameVictory = true;
            endGame();
        }

        /* handle manager states */
        if (gameManagerState == gameOngoing)
        {
            /* update the progress */
            progressTxt.text = progressPrefix + gamesettings.numberOfCellsDestroyedSoFar.ToString() + "/" + gamesettings.winConditionNumberOfCells;
        }
    }

    public static void generateNewCellNumberText()
    {
        int localRandNumber;
        bool foundRightNumber = false;
        int timeout = 0;

        while((!foundRightNumber) && (timeout < randomGenTimeout))
        {
            localRandNumber = Random.Range(gamesettings.lowCellNumber, gamesettings.highCellNumber);

            /* check if there is at least one cell generated with that number */
            if(celloperation.cellExistsForNumber(localRandNumber))
            {
                gamesettings.crtCellNumberToDestroy = localRandNumber;
                generaterandomcellnumber.displayNewRandomCellNumber(localRandNumber);
                foundRightNumber = true;
            }
            else
            {
                timeout++;
            }
        }
        
    }

    public static void initializeGame()
    {
        gameManagerState = gameInit;
    }

    public static void startGame()
    {
        gameManagerState = gameOngoing;
    }

    public static void endGame()
    {
        scenemng.LoadLevel("endgameScene");
        gameManagerState = gameFinished;
    }

    public static bool isGameRunning()
    {
        return (gameManagerState == gameOngoing);
    }

}
