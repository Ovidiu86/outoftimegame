﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class generateantibody : MonoBehaviour
{
    public GameObject antiBodyObject;

    private static bool genAntiBodyReq;

    // Start is called before the first frame update
    void Start()
    {
        genAntiBodyReq = false;
    }

    // Update is called once per frame
    void Update()
    {
        
        if(genAntiBodyReq)
        {
            generateAntiBody();
            genAntiBodyReq = false;
        }

    }

    private void generateAntiBody()
    {
        byte genRandNb;
        float xValue;

        genRandNb = (byte)Random.Range(0f,1.99f);

        if(genRandNb == 0)
        {
            xValue = gamesettings.maxXScreen - gamesettings.antiBodyGenerationPadding;
        }
        else
        {
            xValue = 0 - gamesettings.maxXScreen + gamesettings.antiBodyGenerationPadding;
        }

        generateObject.generateGameObject(antiBodyObject, this.gameObject, new Vector3(xValue, Random.Range(-1, 1), 0), true);
    }

    public static void generateAntiBodyRequest()
    {
        genAntiBodyReq = true;
    }

}
