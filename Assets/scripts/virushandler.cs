﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class virushandler : MonoBehaviour
{

    private Vector3 mousePosition;

    private const float moveSpriteSpeed = 1f;
    private const float padding = 0.5f;
    private const float uppadding = 1f;

    private float minX;
    private float maxX;
    private float minY;
    private float maxY;

    // Start is called before the first frame update
    void Start()
    {

        minX = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0)).x;
        maxX = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0)).x;
        minY = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0)).y;

        maxY = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0)).y - uppadding;
    }

    // Update is called once per frame
    void Update()
    {
        float newX;
        float newY;

        if(gamemanagerscript.isGameRunning())
        {
            Vector2 newposition = transform.position;
            mousePosition = Input.mousePosition;
            mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);

            newX = Mathf.Clamp(mousePosition.x, (minX + padding), (maxX - padding));
            newY = Mathf.Clamp(mousePosition.y, (minY + padding), (maxY - padding));

            transform.position = Vector2.Lerp(transform.position, new Vector2(newX, newY), moveSpriteSpeed);

        }
    }

    private void OnMouseDown()
    {
        gamemanagerscript.startGame();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        gamemanagerscript.endGame();
    }

}
