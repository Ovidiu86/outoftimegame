﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cellscript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        /* check if collision is with the virus sprite */
        if (collision.gameObject.tag == "virus")
        {
            int localCellNumber;

            try
            {
                localCellNumber = celloperation.getNumberForCell(gameObject);

                if(localCellNumber == gamesettings.crtCellNumberToDestroy)
                {
                    celloperation.removeCell(gameObject);
                    if(gamesettings.numberOfActiveCells > 0)
                    {
                        gamesettings.numberOfActiveCells--;
                    }
                    gamesettings.numberOfCellsDestroyedSoFar++;

                    timerscript.resetTimer();

                    /* generate a new cell number to attack */
                    gamemanagerscript.generateNewCellNumberText();

                    Destroy(gameObject);
                }
                else
                {
                    gamemanagerscript.endGame();
                }
            }
            catch(System.ArgumentException e)
            {
                /* normally I shouldn't get in here */
                /* do nothing for the moment */
            }
        }
    }
}
