﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cellObjectType
{
    private GameObject cell;
    private int cellNumber;


    public GameObject Cell
    {
        get
        {
            return cell;
        }
        set
        {
            cell = value;
        }
    }

    public int CellNumber
    {
        get
        {
            return cellNumber;
        }
        set
        {
            cellNumber = value;
        }
    }

    public cellObjectType(GameObject cellobj, int cellnb)
    {
        cell = cellobj;
        cellNumber = cellnb;
    }




}
